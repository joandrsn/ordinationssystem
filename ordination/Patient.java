package ordination;

import java.util.ArrayList;

public class Patient {
	private String cprnr;
	private String navn;
	private double vaegt;
	private ArrayList<Ordination> ordinationer;

	// TODO: Link til Ordination

	public Patient(String cprnr, String navn, double vaegt) {
		super();
		this.cprnr = cprnr;
		this.navn = navn;
		this.vaegt = vaegt;
		ordinationer = new ArrayList<Ordination>();
	}

	public ArrayList<Ordination> getOrdinationer() {
		return new ArrayList<Ordination>(ordinationer);
	}

	public void addOrdination(Ordination ordination) {
		if (!ordinationer.contains(ordination))
			ordinationer.add(ordination);
	}

	public void removeOrdination(Ordination ordination) {
		if (ordinationer.contains(ordination)) {
			ordinationer.remove(ordination);
		}
	}

	public String getCprnr() {
		return cprnr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public double getVaegt() {
		return vaegt;
	}

	public void setVaegt(double vaegt) {
		this.vaegt = vaegt;
	}

	public String toString() {
		return navn + "  " + cprnr;
	}
	// TODO: Metoder (med specifikation) til at vedligeholde link til Ordination
}
