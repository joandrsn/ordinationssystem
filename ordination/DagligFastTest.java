package ordination;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class DagligFastTest {
	
	private Patient p;
	private Laegemiddel l1,l2,l3,l4;
	private Date d1,d2,d3,d4;
	

	@Before
	public void setUp() throws Exception {
		p = new Patient("130707-0707", "Jonas", 24.9);
		l1 = new Laegemiddel("Pinex", 0.1, 0.15, 0.16, "Styk");
		l2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		l3 = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		l4 = new Laegemiddel("ABC", 0.01, 0.015, 0.02, "Styk");
		d1 = dateutil.DateUtil.createDate(2012,02,14);
		d2 = dateutil.DateUtil.createDate(2012,02,15);
		d3 = dateutil.DateUtil.createDate(2012,02,17);
		d4 = dateutil.DateUtil.createDate(2012,02,21);
	}

	@Test
	public void tc1dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 1, 0, 0, 0);
		double DoegnDosis=df.doegnDosis();
		assertEquals(1.0,DoegnDosis,0.001);
		
	}
	
	@Test
	public void tc2dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 0, 0, 0, 0);
		double DoegnDosis=df.doegnDosis();
		assertEquals(0.0,DoegnDosis,0.001);
		
	}

	
	@Test
	public void tc3dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 0, 1, 0, 0);
		double DoegnDosis=df.doegnDosis();
		assertEquals(1.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc4dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 0, 0, 1, 0);
		double DoegnDosis=df.doegnDosis();
		assertEquals(1.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc5dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 0, 0, 0, 1);
		double DoegnDosis=df.doegnDosis();
		assertEquals(1.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc6dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 1, 1, 1, 1);
		double DoegnDosis=df.doegnDosis();
		assertEquals(4.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc7dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 1, 1, 0, 0);
		double DoegnDosis=df.doegnDosis();
		assertEquals(2.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc8dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 0, 0, 1, 1);
		double DoegnDosis=df.doegnDosis();
		assertEquals(2.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc9dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 0, 1, 1, 0);
		double DoegnDosis=df.doegnDosis();
		assertEquals(2.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc10dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 1, 0, 0, 1);
		double DoegnDosis=df.doegnDosis();
		assertEquals(2.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc11dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 1, 1, 1, 0);
		double DoegnDosis=df.doegnDosis();
		assertEquals(3.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc12dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 0, 1, 1, 1);
		double DoegnDosis=df.doegnDosis();
		assertEquals(3.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc13dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 1, 0, 1, 1);
		double DoegnDosis=df.doegnDosis();
		assertEquals(3.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc14dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 1, 1, 0, 1);
		double DoegnDosis=df.doegnDosis();
		assertEquals(3.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc15dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 0, 1, 0, 1);
		double DoegnDosis=df.doegnDosis();
		assertEquals(2.0,DoegnDosis,0.001);
		
	}
	
	
	@Test
	public void tc16dd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 1, 0, 1, 0);
		double DoegnDosis=df.doegnDosis();
		assertEquals(2.0,DoegnDosis,0.001);
		
	}
	
	@Test
	public void tc1sd() {
		DagligFast df = new DagligFast(d1, d1, p, l1, 0, 0, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(0.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc2sd() {
		DagligFast df = new DagligFast(d1, d1, p, l1, 1, 0, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(1.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc3sd() {
		DagligFast df = new DagligFast(d1, d1, p, l1, 1, 1, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(2.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc4sd() {
		DagligFast df = new DagligFast(d1, d1, p, l1, 1, 1, 1, 1);
		double SamletDosis=df.samletDosis();
		assertEquals(4.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc5sd() {
		DagligFast df = new DagligFast(d1, d1, p, l1, 2, 2, 2, 2);
		double SamletDosis=df.samletDosis();
		assertEquals(8.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc6sd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 0, 0, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(0.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc7sd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 1, 0, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(2.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc8sd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 1, 1, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(4.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc9sd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 1, 1, 1, 1);
		double SamletDosis=df.samletDosis();
		assertEquals(8.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc10sd() {
		DagligFast df = new DagligFast(d1, d2, p, l1, 2, 2, 2, 2);
		double SamletDosis=df.samletDosis();
		assertEquals(16.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc11sd() {
		DagligFast df = new DagligFast(d1, d3, p, l1, 0, 0, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(0.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc12sd() {
		DagligFast df = new DagligFast(d1, d3, p, l1, 1, 0, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(4.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc13sd() {
		DagligFast df = new DagligFast(d1, d3, p, l1, 1, 1, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(8.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc14sd() {
		DagligFast df = new DagligFast(d1, d3, p, l1, 1, 1, 1, 1);
		double SamletDosis=df.samletDosis();
		assertEquals(16.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc15sd() {
		DagligFast df = new DagligFast(d1, d3, p, l1, 2, 2, 2, 2);
		double SamletDosis=df.samletDosis();
		assertEquals(32.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc16sd() {
		DagligFast df = new DagligFast(d1, d4, p, l1, 0, 0, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(0.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc17sd() {
		DagligFast df = new DagligFast(d1, d4, p, l1, 1, 0, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(8.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc18sd() {
		DagligFast df = new DagligFast(d1, d4, p, l1, 1, 1, 0, 0);
		double SamletDosis=df.samletDosis();
		assertEquals(16.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc19sd() {
		DagligFast df = new DagligFast(d1, d4, p, l1, 1, 1, 1, 1);
		double SamletDosis=df.samletDosis();
		assertEquals(32.0,SamletDosis,0.001);
	
	}
	
	@Test
	public void tc20sd() {
		DagligFast df = new DagligFast(d1, d4, p, l1, 2, 2, 2, 2);
		double SamletDosis=df.samletDosis();
		assertEquals(64.0,SamletDosis,0.001);
	
	}
	
	
	
	
	














}
