package ordination;

import java.util.Arrays;
import java.util.Date;

public class DagligFast extends Ordination {

	private Dosis[] doser = new Dosis[4];

	public DagligFast(Date startDen, Date slutDen, Patient patient, Laegemiddel laegemiddel, double morgenAntal,
			double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen, laegemiddel, patient);
		if (morgenAntal > 0) {
			doser[0] = createDosis("Morgen", morgenAntal);
		}
		if (middagAntal > 0) {
			doser[1] = createDosis("Middag", middagAntal);
		}
		if (aftenAntal > 0) {
			doser[2] = createDosis("Aften", aftenAntal);
		}
		if (natAntal > 0) {
			doser[3] = createDosis("Nat", natAntal);
		}
	}

	public Dosis[] getDoser() {
		return doser;
	}

	public Dosis createDosis(String tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		return dosis;

	}

	@Override
	public double samletDosis() {
		return (antalDage()+1) * doegnDosis();
	}

	@Override
	public double doegnDosis() {
		double doegndosis = 0;
		for (int i = 0; i < doser.length; i++) {
			if (doser[i] != null) {
				doegndosis = doegndosis + doser[i].getAntal();
			}
		}
		return doegndosis;
	}

}
