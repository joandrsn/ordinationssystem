package ordination;

import java.util.ArrayList;
import java.util.Date;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> doser;

	public DagligSkaev(Date startDen, Date slutDen, Laegemiddel laegemiddel, Patient patient) {
		super(startDen, slutDen, laegemiddel, patient);
		doser = new ArrayList<Dosis>();

	}

	public void opretDosis(String tidspunkt, double antal) {
		Dosis dose = new Dosis(tidspunkt, antal);
		doser.add(dose);
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<Dosis>(doser);
	}

	public void deleteDosis(Dosis dose) {
		if (doser.contains(dose)) {
			doser.remove(dose);
		}
	}

	@Override
	public double samletDosis() {
		double result = 0;
		for (Dosis d : doser) {
			result += d.getAntal();
		}
		result = result * (this.antalDage() + 1);
		return result;
	}

	@Override
	public double doegnDosis() {
		double result = 0;
		result = samletDosis() / (this.antalDage() + 1);
		return result;
	}
}
