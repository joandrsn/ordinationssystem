package ordination;

public class Laegemiddel {
	private String navn;
	private double enhedPrKgPrDoegnLet; // faktor der anvendes hvis patient
										// vejer < 25 kg
	private double enhedPrKgPrDoegnNormal;// faktor der anvendes hvis 25 kg <=
											// patient v�gt <= 120 kg
	private double enhedPrKgPrDoegnTung; // faktor der anvendes hvis patient
											// v�gt > 120 kg
	private String enhed;
	

	/**
	 * Opretter et l�gemiddel. Krav: navn er ikke tom, enhedPrKgPrD�gnLet >= 0,
	 * enhedPrKgPrD�gnNormal >=0, enhedPrKgPrD�gnTung >=0, enhed er ["stk",
	 * "pust", "mL", "dr�ber"].
	 */
	public Laegemiddel(String navn, double enhedPrKgPrDoegnLet,
			double enhedPrKgPrDoegnNormal, double enhedPrKgPrDoegnTung,
			String enhed) {
		super();
		this.navn = navn;
		this.enhedPrKgPrDoegnLet = enhedPrKgPrDoegnLet;
		this.enhedPrKgPrDoegnNormal = enhedPrKgPrDoegnNormal;
		this.enhedPrKgPrDoegnTung = enhedPrKgPrDoegnTung;
		this.enhed = enhed;
	}

	public String getEnhed() {
		return enhed;
	}

	public String getNavn() {
		return navn;
	}

	public double getEnhePrKgPrDoegnLet() {
		return enhedPrKgPrDoegnLet;
	}

	public double getEnhePrKgPrDoegnNormal() {
		return enhedPrKgPrDoegnNormal;
	}

	public double getEnhePrKgPrDoegnTung() {
		return enhedPrKgPrDoegnTung;
	}

	public String toString() {
		return navn;
	}
}
