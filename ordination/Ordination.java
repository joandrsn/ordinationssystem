package ordination;

import java.util.Date;
import dateutil.DateUtil;

public abstract class Ordination {
	private Date startDen;
	private Date slutDen;
	private Laegemiddel laegemiddel;

	// TODO Link til L�gemiddel
	// TODO constructor (med specifikation)

	public Ordination(Date startDen, Date slutDen, Laegemiddel laegemiddel, Patient patient) {
		this.startDen = startDen;
		this.slutDen = slutDen;
		this.laegemiddel = laegemiddel;
		patient.addOrdination(this);
	}

	public Laegemiddel getLaegemiddel() {
		return laegemiddel;
	}

	public void setLaegemiddel(Laegemiddel laegemiddel) {
		if (this.laegemiddel != laegemiddel)
			this.laegemiddel = laegemiddel;
	}

	public Date getStartDen() {
		return startDen;
	}

	public Date getSlutDen() {
		return slutDen;
	}

	/**
	 * Returnerer antal hele dage mellem startdato og slutdato. Begge dage
	 * inklusive. Krav: startdato<=slutdato
	 * 
	 * @return antal dage ordinationen g�lder for
	 */
	public int antalDage() {
		return DateUtil.daysDiff(startDen, slutDen);
	}

	public String toString() {
		return DateUtil.dateToString(startDen);
	}

	// TODO: Metoder(med specifikation) til at vedligeholde link til L�gemiddel

	/**
	 * Returnerer den totale dosis der er givet i den periode ordinationen er
	 * gyldig
	 * 
	 * @return
	 */
	public abstract double samletDosis();

	/**
	 * Returnerer den gennemsnitlige dosis givet pr dag i den periode
	 * ordinationen er gyldig
	 * 
	 * @return
	 */
	public abstract double doegnDosis();

}
