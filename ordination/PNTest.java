package ordination;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class PNTest {

	private PN pn1, pn2, pn3, pn4;
	private Date d1, d2, d3, d4, d5, d6;
	private Patient p1;
	private Laegemiddel middel;

	@Before
	public void setUp() throws Exception {
		d1 = dateutil.DateUtil.createDate(2013, 3, 1);
		d2 = dateutil.DateUtil.createDate(2012, 3, 26);
		d3 = dateutil.DateUtil.createDate(2012, 3, 25);
		d4 = dateutil.DateUtil.createDate(2012, 3, 22);
		d5 = dateutil.DateUtil.createDate(2012, 3, 21);
		d6 = dateutil.DateUtil.createDate(2012, 1, 1);
		middel = new Laegemiddel("Pinex", 1, 2, 3, "Styk");
		p1 = new Patient("123456-2324", "Lene Lenesen", 70);
		pn1 = new PN(d5, d2, middel, 10, p1);
		pn1.givDosis(d2);
		pn1.givDosis(d3);
		pn1.givDosis(d5);
		pn2 = new PN(d6, d2, middel, 10, p1);
		pn2.givDosis(d5);
		pn2.givDosis(d4);
		pn2.givDosis(d3);
		pn2.givDosis(d2);
		pn2.givDosis(d6);
		pn2.givDosis(d2);
		pn2.givDosis(d3);
		pn2.givDosis(d4);
		pn2.givDosis(d5);
		pn3 = new PN(d6, d2, middel, 10, p1);
		pn4 = new PN(d1, d2, middel, 10, p1);
		// System.out.println(dateutil.DateUtil.daysDiff(d6, d2));

	}

	// test for at man ikke kan give piller udenfor perioden hvor patienten er
	// indlagt
	@Test
	public void test1() {
		assertEquals(false, pn1.givDosis(d6));
	}

	// test for at man ikke kan give piller udenfor perioden hvor patienten er
	// indlagt
	@Test
	public void test2() {
		assertEquals(false, pn1.givDosis(d1));
	}

	// test for at den samlede dose bereges korrekt
	@Test
	public void test3() {
		assertEquals(30, pn1.samletDosis(), 0.1);
	}

	// test for at døgndosis beregnes korrekt
	@Test
	public void test4() {
		assertEquals(30 / 5, pn1.doegnDosis(), 0.1);
	}

	// test for at antal gange givet virker korrekt
	@Test
	public void test5() {
		assertEquals(3, pn1.getAntalGangeGivet());
	}

	// test for at antal enheder pr dosis beregnes korrekt
	@Test
	public void test6() {
		assertEquals(10, pn1.getAntalEnheder(), 0.1);
	}

	// test for at døgndosis beregnes korrekt
	@Test
	public void test7() {
		assertEquals((90 / 84), pn2.doegnDosis(), 0.1);
	}

	// test for at samlet dose beregnes korrekt
	@Test
	public void test8() {
		assertEquals(90, pn2.samletDosis(), 0.1);
	}

	// test for at antal gange givet virker korrekt
	@Test
	public void test9() {
		assertEquals(9, pn2.getAntalGangeGivet());
	}

	// test for at antal enheder pr dosis beregnes korrekt
	@Test
	public void test10() {
		assertEquals(10, pn2.getAntalEnheder(), 0.1);
	}

	// test for at givDosis virker
	@Test
	public void test11() {
		assertEquals(true, pn3.givDosis(d4));
	}

	// test for at givDosis virker
	@Test
	public void test12() {
		assertEquals(false, pn3.givDosis(d1));
	}

	// test på constructor
	@Test
	public void test13() {
		PN pnTEMP = new PN(d1, d2, middel, 10, p1);
		assertNotNull(pnTEMP);
	}

	// test på at felter bliver sat
	@Test
	public void test14() {
		assertEquals(10, pn4.getAntalEnheder(), 0.1);
		assertEquals(middel, pn4.getLaegemiddel());
		assertEquals(d1, pn4.getStartDen());
		assertEquals(d2, pn4.getSlutDen());
	}
}
