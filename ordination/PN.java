package ordination;

import java.util.ArrayList;
import java.util.Date;

import dateutil.DateUtil;

public class PN extends Ordination {

	private double dosis;
	private ArrayList<Date> datoFået;

	public PN(Date startDen, Date slutDen, Laegemiddel laegemiddel, double enhederTotal, Patient patient) {
		super(startDen, slutDen, laegemiddel, patient);
		datoFået = new ArrayList<Date>();
		this.dosis = enhederTotal;

	}

	/**
	 * Registrerer at der er givet en dosis p� dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers - dato en givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(Date givesDen) {
		boolean result;
		if (givesDen.compareTo(getSlutDen()) <= 0 && givesDen.compareTo(getStartDen()) >= 0) {
			result = true;
			datoFået.add(givesDen);
		} else {
			result = false;
		}
		return result;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		// TODO
		return datoFået.size();
	}

	public double getAntalEnheder() {
		// TODO Auto-generated method stub
		return dosis;
	}

	@Override
	public double samletDosis() {
		// TODO Auto-generated method stub
		return datoFået.size() * dosis;
	}

	@Override
	public double doegnDosis() {
		// TODO Auto-generated method stub
		double result = samletDosis();

		if (datoFået.size() > 0) {
			Date firstDate = datoFået.get(0);
			Date lastDate = datoFået.get(0);
			for (int i = 1; i < datoFået.size(); i++) {
				if (firstDate.compareTo(datoFået.get(i)) > 0) {
					firstDate = datoFået.get(i);
				}
				if (lastDate.compareTo(datoFået.get(i)) < 0) {
					lastDate = datoFået.get(i);
				}

			}
			// System.out.println(firstDate);
			// System.out.println(lastDate);
			int days = DateUtil.daysDiff(firstDate, lastDate) + 1;
			// System.out.println(days);
			if (days > 0) {
				result = result / days;
			}

			// int dage = datoFået.get(antalEnheder -
			// 1).compareTo(datoFået.get(0));
			// if (dage > 0) {
			//
			// result = result / (dage + 1);
			//
			// }

		}

		return result;
	}
}
