package ordination;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {

	private DagligSkaev ds1, ds2, ds3, ds4, ds5;
	private Date d1, d2, d3, d4, d5, d6;
	private Patient p1;
	private Laegemiddel middel;

	@Before
	public void setUp() throws Exception {
		d1 = dateutil.DateUtil.createDate(2013, 3, 1);
		d2 = dateutil.DateUtil.createDate(2012, 3, 26);
		d3 = dateutil.DateUtil.createDate(2012, 3, 25);
		d4 = dateutil.DateUtil.createDate(2012, 3, 22);
		d5 = dateutil.DateUtil.createDate(2012, 3, 21);
		d6 = dateutil.DateUtil.createDate(2012, 1, 1);
		middel = new Laegemiddel("Pinex", 1, 2, 3, "Styk");
		p1 = new Patient("123456-2324", "Lene Lenesen", 70);
		ds1 = new DagligSkaev(d6, d2, middel, p1);
		ds1.opretDosis("04:00", 4);
		ds1.opretDosis("10:00", 4);
		ds1.opretDosis("15:00", 6);
		ds1.opretDosis("20:00", 3);
		ds1.opretDosis("23:59", 2);
		ds2 = new DagligSkaev(d6, d2, middel, p1);
		ds3 = new DagligSkaev(d4, d3, middel, p1);
		ds3.opretDosis("23:59", 50);
		ds3.opretDosis("11:00", 25);
		ds3.opretDosis("15:00", 25);
		ds4 = new DagligSkaev(d4, d4, middel, p1);
		ds4.opretDosis("11:00", 10);
		ds5 = new DagligSkaev(d4, d4, middel, p1);

	}

	// test på om opret dosis virker, gyldigt tidspunkt
	@Test
	public void test1() {
		ds2.opretDosis("12:59", 10);
		ds2.opretDosis("24:00", 12);
		assertEquals(2, ds2.getDoser().size());
	}

	// test om opret dosis virker med ugyldige tidspunkter
	@Test
	public void test2() {
		ds2.opretDosis("59:59", 10);
		ds2.opretDosis("abcde", 10);
		assertEquals(2, ds2.getDoser().size());
	}

	// test over samlet dosis
	@Test
	public void test3() {
		assertEquals(300, ds3.samletDosis(), 0.1);
	}

	// test over samet dosis
	@Test
	public void test4() {
		assertEquals(10, ds4.samletDosis(), 0.1);
	}

	// test over delete dose
	@Test
	public void test5() {
		Dosis do3 = ds4.getDoser().get(0);
		assertEquals(true, ds4.getDoser().contains(do3));
		ds4.deleteDosis(do3);
		assertEquals(0, ds4.getDoser().size());
	}

	// test over doegnDosis
	@Test
	public void test6() {
		assertEquals(100, ds3.doegnDosis(), 0.1);
	}

	// test over doegnDosis
	@Test
	public void test7() {
		assertEquals(19, ds1.doegnDosis(), 0.1);
	}

	// test over doegnDosis
	@Test
	public void test8() {
		assertEquals(0, ds5.doegnDosis(), 0.1);
	}
	
	// test på constructor
	@Test
	public void test9(){
		assertNotNull(new DagligSkaev(d2, d5, middel, p1));
	}

}
