package ordination;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class OrdinationTest {

	private Ordination o1, o2, o3;
	private Date d1, d2, d3, d4, d5, d6;
	private Patient p1;
	private Laegemiddel middel, middel2;

	@Before
	public void setUp() throws Exception {
		d1 = dateutil.DateUtil.createDate(2013, 3, 1);
		d2 = dateutil.DateUtil.createDate(2012, 3, 26);
		d3 = dateutil.DateUtil.createDate(2012, 3, 25);
		d4 = dateutil.DateUtil.createDate(2012, 3, 22);
		d5 = dateutil.DateUtil.createDate(2012, 3, 21);
		d6 = dateutil.DateUtil.createDate(2012, 1, 1);
		middel = new Laegemiddel("Pinex", 1, 2, 3, "Styk");
		middel2 = new Laegemiddel("TESTMIDDEL", 3, 2, 1, "Ml");
		p1 = new Patient("123456-2324", "Lene Lenesen", 70);
		o1 = new DagligFast(d6, d1, p1, middel, 1, 2, 3, 4);
		o2 = new DagligFast(d6, d1, p1, null, 0, 0, 0, 0);
		o3 = new DagligFast(d1, d1, p1, null, 0, 0, 0, 0);

	}

	// test på constructor
	@Test
	public void test1() {
		assertNotNull(o1);
	}

	// test på at slutdato bliver sat
	@Test
	public void test2() {
		assertEquals(d1, o1.getSlutDen());
	}

	// test på startto bliver sat
	@Test
	public void test3() {
		assertEquals(d6, o1.getStartDen());
	}

	// test på at lægemidlet bliver sat
	@Test
	public void test4() {
		assertEquals(middel, o1.getLaegemiddel());
	}

	// test på at antal dage passer
	@Test
	public void test5() {
		assertEquals(425, o1.antalDage());
	}

	// test på at toString metoden bliver lavet rigtigt
	@Test
	public void test6() {
		assertEquals(dateutil.DateUtil.dateToString(d6), o1.toString());
	}

	// test på at setLaegemiddel virker korrekt
	@Test
	public void test7() {
		assertNull(o2.getLaegemiddel());
		o2.setLaegemiddel(middel2);
		assertEquals(middel2, o2.getLaegemiddel());
	}

	// test på at antal dage passer når ind og ud dato er den samme
	@Test
	public void test8() {
		assertEquals(0, o3.antalDage());
	}
	
	//test på at patient ikke kan være null
	@Test(expected = NullPointerException.class)
	public void test9(){
		new DagligFast(d1, d1, null, null, 0, 0, 0, 0);
	}
}
