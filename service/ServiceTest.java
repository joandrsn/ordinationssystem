package service;

import static org.junit.Assert.*;

import java.util.Date;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

import org.junit.Before;
import org.junit.Test;
import service.Service;

public class ServiceTest {
	
	private Patient p;
	private Laegemiddel l1,l2,l3,l4;
	private Date d1,d2,d3;
	private String []kl;
	private double[]dosis;
	
	@Before
	public void setUp() throws Exception {
		p = new Patient("130707-0707", "Jonas", 24.9);
		l1 = new Laegemiddel("Pinex", 0.1, 0.15, 0.16, "Styk");
		l2 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		l3 = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		l4 = new Laegemiddel("ABC", 0.01, 0.015, 0.02, "Styk");
		d1 = dateutil.DateUtil.createDate(2012,02,14);
		d2 = dateutil.DateUtil.createDate(2012,02,15);
		d3 = dateutil.DateUtil.createDate(2012,02,20);
		kl= new String[]{"00:00"};
		dosis=new double[]{8.0};
		

	}

	@Test
	public void tc1() {
		assertEquals((24.9*0.1), Service.anbefaletDosisPrDøgn(p, l1),0.0001);
		
	}
	
	@Test
	public void tc2() {
		p.setVaegt(25);
		assertEquals((25*0.15), Service.anbefaletDosisPrDøgn(p, l1),0.0001);
		
	}
	
	@Test
	public void tc3() {
		p.setVaegt(120);
		assertEquals((120*0.15), Service.anbefaletDosisPrDøgn(p,l1),0.0001);
		
	}
	
	@Test
	public void tc4() {
		p.setVaegt(120.1);
		assertEquals((120.1*0.16), Service.anbefaletDosisPrDøgn(p, l1),0.0001);
		
	}
	
	@Test
	public void tc5() {
		assertEquals((24.9*1), Service.anbefaletDosisPrDøgn(p,l2),0.0001);
		
	}
	
	@Test
	public void tc6() {
		p.setVaegt(25);
		assertEquals((25*1.5), Service.anbefaletDosisPrDøgn(p, l2),0.0001);
		
	}
	
	@Test
	public void tc7() {
		p.setVaegt(120);
		assertEquals((120*1.5), Service.anbefaletDosisPrDøgn(p, l2),0.0001);
		
	}
	
	@Test
	public void tc8() {
		p.setVaegt(120.1);
		assertEquals((120.1*2), Service.anbefaletDosisPrDøgn(p, l2),0.0001);
		
	}
	
	@Test
	public void tc9() {
		assertEquals((24.9*0.025), Service.anbefaletDosisPrDøgn(p, l3),0.0001);
		
	}
	
	@Test
	public void tc10() {
		p.setVaegt(25);
		assertEquals((25*0.025), Service.anbefaletDosisPrDøgn(p, l3),0.0001);
		
	}
	
	@Test
	public void tc11() {
		p.setVaegt(120);
		assertEquals((120*0.025), Service.anbefaletDosisPrDøgn(p, l3),0.0001);
		
	}
	
	@Test
	public void tc12() {
		p.setVaegt(120.1);
		assertEquals((120.1*0.025), Service.anbefaletDosisPrDøgn(p, l3),0.0001);
		
	}
	
	@Test
	public void tc13() {
		assertEquals((24.9*0.01), Service.anbefaletDosisPrDøgn(p, l4),0.0001);
		
	}
	
	@Test
	public void tc14() {
		p.setVaegt(25);
		assertEquals((25*0.015), Service.anbefaletDosisPrDøgn(p, l4),0.0001);
		
	}
	
	@Test
	public void tc15() {
		p.setVaegt(120);
		assertEquals((120*0.015), Service.anbefaletDosisPrDøgn(p, l4),0.0001);
		
	}
	
	@Test
	public void tc16() {
		p.setVaegt(120.1);
		assertEquals((120.1*0.02), Service.anbefaletDosisPrDøgn(p, l4),0.0001);
		
	}
	
	@Test
	public void tc17() {
		p.setVaegt(0);
		assertEquals((0), Service.anbefaletDosisPrDøgn(p, l1),0.0001);
		
	}
	
	@Test
	public void tc18() {
		p.setVaegt(0);
		assertEquals((0), Service.anbefaletDosisPrDøgn(p, l2),0.0001);
		
	}
	
	@Test
	public void tc19() {
		p.setVaegt(0);
		assertEquals((0), Service.anbefaletDosisPrDøgn(p,l3),0.0001);
		
	}
	
	@Test
	public void tc20() {
		p.setVaegt(0);
		assertEquals((0), Service.anbefaletDosisPrDøgn(p, l4),0.0001);
		
	}
	
	@Test
	public void tc21(){
		DagligFast df = Service.opretDagligFastOrdination(d2, d3, p, l1, 1, 0, 0, 0);
		assertNotNull(df);
	}
	
	@Test(expected = RuntimeException.class)
	public void tc22(){
		DagligFast df = Service.opretDagligFastOrdination(d2, d3, p, l1, -1, 0, 0, 0);
		
	}
	
	@Test(expected = RuntimeException.class)
	public void tc23(){
		DagligFast df = Service.opretDagligFastOrdination(d2, d1, p, l1, 1, 0, 0, 0);
		
	}
	
	@Test(expected = RuntimeException.class)
	public void tc24(){
		PN p1 = Service.opretPNOrdination(d2, d3, p, l1, 0);
		
		
	}
	
	@Test(expected = RuntimeException.class)
	public void tc25(){
		PN p1 = Service.opretPNOrdination(d2, d1, p, l1, 10);
		
	}
	
	@Test(expected = RuntimeException.class)
	public void tc26(){
		PN p1 = Service.opretPNOrdination(d2, d3, p, null, 10);
	}

	
	@Test
	public void tc27(){
		
		DagligSkaev da1=Service.opretDagligSkævOrdination(d1, d3, p, l1,kl,dosis);
		assertNotNull(da1);
		
	}
	
	@Test(expected =RuntimeException.class)
	public void tc28(){
		DagligSkaev da1=Service.opretDagligSkævOrdination(d2, d1, p, l1,kl,dosis);
		
		
	}
	
	@Test(expected =RuntimeException.class)
	public void tc29(){
		DagligSkaev da1=Service.opretDagligSkævOrdination(d1, d2, p, null,kl,dosis);
	}
	
	
	

}
