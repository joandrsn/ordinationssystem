package service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import dao.Dao;
import dateutil.DateUtil;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class Service {

	/**
	 * Opretter PN ordination
	 * 
	 * @param startDen
	 * @param slutDen
	 * @param patient
	 * @param laegemiddel
	 * @param antal
	 * @return opretter og returnerer en PN ordination Hvis startDato er efter
	 *         slutDato kastes en RuntimeException og ordinationen oprettes ikke
	 */
	public static PN opretPNOrdination(Date startDen, Date slutDen, Patient patient, Laegemiddel laegemiddel,
			double antal) {
		if (DateUtil.daysDiff(startDen, slutDen) < 0) {
			throw new RuntimeException("Startdato er efter slutdato");
		}else if(antal<=0){
			throw new RuntimeException("Dosis Antal ugyldigt(<=0)");
		}else if(laegemiddel==null){
			throw new RuntimeException("der er ikke valgt et laegemiddel");
		}
		PN aktuelPN = new PN(startDen, slutDen, laegemiddel, antal, patient);
		
		return aktuelPN;
	}

	/**
	 * Opretter DagligFast ordination
	 * 
	 * @param startDen
	 * @param slutDen
	 * @param patient
	 * @param laegemiddel
	 * @param morgenAntal
	 * @param middagAntal
	 * @param aftenAntal
	 * @param natAntal
	 * @return opretter og returnerer en DagligFast ordination Hvis startDato er
	 *         efter slutDato kastes en RuntimeException og ordinationen
	 *         oprettes ikke
	 */

	public static DagligFast opretDagligFastOrdination(Date startDen, Date slutDen, Patient patient,
			Laegemiddel laegemiddel, double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		if (DateUtil.daysDiff(startDen, slutDen) < 0) {
			throw new RuntimeException("Startdato er efter slutdato");
		}else if(morgenAntal<0 || middagAntal<0 ||aftenAntal<0 ||natAntal<0){
			throw new RuntimeException("dosisantal er ikke gyldigt (<0)");
		}
		
		DagligFast d = new DagligFast(startDen, slutDen, patient, laegemiddel, morgenAntal, middagAntal, aftenAntal,
				natAntal);
		// TODO
		
		return d;
	}

	/**
	 * Opretter daglig skæv ordination Krav: antalEnheder og klokkeSlæt har
	 * samme længde. klokkeSlæt indeholder tidspunkter på dagen på formen 15:30
	 * 
	 * @param startDen
	 * @param slutDen
	 * @param patient
	 * @param laegemiddel
	 * @param klokkeSlet
	 * @param antalEnheder
	 * @return opretter og returnerer en DagligSkæv ordination Hvis startDato er
	 *         efter slutDato kastes en RuntimeException og ordinationen
	 *         oprettes ikke
	 */
	public static DagligSkaev opretDagligSkævOrdination(Date startDen, Date slutDen, Patient patient,
			Laegemiddel laegemiddel, String[] klokkeSlet, double[] antalEnheder) {
		
		if (DateUtil.daysDiff(startDen, slutDen) < 0) {
			throw new RuntimeException("Ugyldig startDato");
		}else if(laegemiddel==null){
			throw new RuntimeException("der er ikke valgt et laegemiddel");
		}

		DagligSkaev aktuelSkaev = new DagligSkaev(startDen, slutDen, laegemiddel, patient);
		for (int i = 0; i < klokkeSlet.length; i++) {
			aktuelSkaev.opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
		
		return aktuelSkaev;
	}

	/**
	 * Registrerer at PN ordinationen er anvendt på dagen dato
	 * 
	 * @param ordination
	 * @param dato
	 *            En dato for hvornår ordinationen anvendes tilføjes
	 *            ordinationen. Hvis datoen ikke er indenfor ordinationens
	 *            gyldighedsperiode kastes en RuntimeException
	 */
	public static void ordinationPNAnvendt(PN ordination, Date dato) {
		// TODO
		if (ordination.givDosis(dato)) {

		} else {
			throw new RuntimeException("Datoen er ikke i et gyldigt område");
		}
	}

	/**
	 * s
	 * 
	 * @param patient
	 * @param laegemiddel
	 * @return den anbefalede dosis for den pågældende patient(der skal tages
	 *         hensyn til patientens vægt) Det er en forskellig enheds faktor
	 *         der skal anvendes, den er afh�ngig af patinetens vægt
	 */

	public static double anbefaletDosisPrDøgn(Patient patient, Laegemiddel laegemiddel) {
		double result = 0;
		if (patient.getVaegt() < 25)
			result = patient.getVaegt() * laegemiddel.getEnhePrKgPrDoegnLet();
		else if (patient.getVaegt() > 120)
			result = patient.getVaegt() * laegemiddel.getEnhePrKgPrDoegnTung();
		else
			result = patient.getVaegt() * laegemiddel.getEnhePrKgPrDoegnNormal();
		return result;
	}

	public static List<Patient> getAllPatienter() {
		return Dao.getAllPatienter();
	}

	public static List<Laegemiddel> getAllLaegemidler() {
		return Dao.getAllLaegemidler();
	}

	public static void createSomeObjects() {

		Dao.gemPatient(new Patient("Jane Jensen", "121256-0512", 63.4));
		Dao.gemPatient(new Patient("Finn Madsen", "070985-1153", 83.2));

		Dao.gemLaegemiddel(new Laegemiddel("Pinex", 0.1, 0.15, 0.16, "Styk"));
		Dao.gemLaegemiddel(new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml"));
		Dao.gemLaegemiddel(new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk"));
		Dao.gemLaegemiddel(new Laegemiddel("ABC", 0.01, 0.015, 0.02, "Styk"));

		opretPNOrdination(DateUtil.createDate(2012, 3, 1), DateUtil.createDate(2012, 3, 12),
				Dao.getAllPatienter().get(0), Dao.getAllLaegemidler().get(1), 123);
		opretPNOrdination(DateUtil.createDate(2012, 3, 12), DateUtil.createDate(2012, 3, 13), Dao.getAllPatienter()
				.get(0), Dao.getAllLaegemidler().get(0), 3);

		opretDagligFastOrdination(DateUtil.createDate(2012, 1, 10), DateUtil.createDate(2012, 1, 12), Dao
				.getAllPatienter().get(1), Dao.getAllLaegemidler().get(1), 2, -1, 1, -1);
		String[] kl = { "12:00", "12:40", "16:00", "18:45" };
		double[] an = { 0.5, 1, 2.5, 3 };
		opretDagligSkævOrdination(DateUtil.createDate(2012, 1, 23), DateUtil.createDate(2012, 1, 24), Dao
				.getAllPatienter().get(1), Dao.getAllLaegemidler().get(2), kl, an);

	}

}
